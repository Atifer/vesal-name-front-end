export default {
  attributes: {
    usernameOrEmail: 'نام کاربری یا ایمیل',
    password: 'گذرواژه',
    name: 'نام'
  }
}
