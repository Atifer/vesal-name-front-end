import Vue from 'vue'
import {
  ValidationObserver,
  ValidationProvider,
  withValidation,
  Validator
} from 'vee-validate'
import faVeeValidateLocale from 'vee-validate/dist/locale/fa'
import faVeeValidateAttributes from '@/locales/fa-vee-validate-attributes'

Vue.component('ValidationObserver', ValidationObserver)
Vue.component('ValidationProvider', ValidationProvider)
Vue.component('withValidation', withValidation)
Validator.localize('fa', faVeeValidateLocale)
Validator.localize('fa', faVeeValidateAttributes)
