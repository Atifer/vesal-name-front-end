import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import '@fortawesome/fontawesome-free/css/all.css' // Ensure you are using css-loader

export default ({ app }) => {
  Vue.use(Vuetify, {
    rtl: true,
    iconfont: 'fa',
    theme: {
      primary: '#ECB267',
      secondary: '#CB8E3F',
      accent: '#47A379',
      error: '#ff101f',
      warning: '#f8c537',
      info: '#537d8d',
      success: '#244724'
    },
    lang: {
      t: (key, ...params) => app.i18n.t(key, params)
    }
  })
}
